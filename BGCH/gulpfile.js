/// <binding ProjectOpened='watch' />
const { src, dest, watch, series, parallel } = require('gulp');

var concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	minifyCSS = require('gulp-minify-css'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer');

const print = require('gulp-print').default;
const minifyJs = require('gulp-minify');
const babel = require('gulp-babel');

var stylesOut = "assets/css";
var stylesIn = "development/scss/*.scss";
var faStylesIn = "development/scss/fa/*.scss";
var materialStylesIn = "development/scss/material/**/*.scss";
var scriptsIn = "'assets/js/*.js', '!assets/js/donorList.js', '!assets/dist/js/app.js', '!assets/dist/js/app.min.js', '!assets/dist/js/donorList.js'";
const jsOut = "assets/dist/js";

var sassOptions = {
	errLogToConsole: true,
	outputStyle: 'compressed'
};

function concatScripts() {
	return src(["assets/js/debouncedResize.js",
		"assets/js/slick.min.js",
		"assets/js/throttledScroll.js",
		"assets/js/main.js"])
		.pipe(concat('app.js'))
		.pipe(print())                        // #2. print each file in the stream
		.pipe(babel({ presets: [['@babel/preset-env']] }))                      // #3. transpile ES2015 to ES5 using ES2015 preset
		.on('error', console.error.bind(console))
		.pipe(rename('app.js'))
		.pipe(dest(jsOut))              // #4. copy the results to the build folder
		// NOW let's make the min version
		.pipe(minifyJs({ ext: { min: '.min.js' } }))
		.pipe(dest(jsOut));
};

function minifyGoogleMaps() {
	return src('assets/js/map.js')
		.pipe(uglify())
		.pipe(rename('map.min.js'))
		.pipe(dest('assets/dist/js'));
};

function concatCSS() {
	return src(["assets/css/slick.css",
		"assets/css/slick-theme.css",
		"assets/css/materialize.css",
		"assets/css/styles.css",
		"assets/css/font-awesome.css"])
		.pipe(minifyCSS())
		.pipe(concat('styles.min.css'))
		.pipe(dest('assets/css'));
};

function sass() {
	return src([stylesIn, faStylesIn, materialStylesIn])
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(dest(stylesOut));
};

// Watch

function watchTask() {
	watch(
		[stylesIn, faStylesIn, materialStylesIn, 'assets/js/*.js', '!assets/js/donorList.js', '!assets/dist/js/app.js', '!assets/dist/js/app.min.js', '!assets/dist/js/donorList.js'],
		series(sass, concatCSS, concatScripts, minifyGoogleMaps)
	);
}

// Default task

exports.default = series(watchTask);

exports.styles = series(sass);
exports.scripts = series(concatScripts, minifyGoogleMaps);
/// <binding ProjectOpened='watch' />
const concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	minifyCSS = require('gulp-minify-css'),
	gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('gulp-autoprefixer'),
	livereload = require('gulp-livereload');


const stylesOut = "assets/css";
const stylesIn = "development/scss/*.scss";
const faStylesIn = "development/scss/fa/*.scss";
const materialStylesIn = "development/scss/material/**/*.scss";

const sassOptions = {
	errLogToConsole: true,
	outputStyle: 'compressed'
};

function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./_site/"
        },
        port: 3000
    });
    done();
}

function browserSyncReload(done) {
    browsersync.reload();
    done();
}

function concatScripts() {
	return gulp.src(["assets/js/debouncedResize.js",
		"assets/js/slick.min.js",
		"assets/js/throttledScroll.js",
		"assets/js/main.js"])
		.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(rename('app.min.js'))
		.pipe(sourcemaps.write(''))
        .pipe(gulp.dest('assets/dist/js'))
        .pipe(browsersync.stream());
};

//gulp.task('minifyScripts', ['concatScripts'], function(){
//gulp.src('assets/dist/js/app.js')
//.pipe(sourcemaps.init())
//.pipe(uglify())
//.pipe(sourcemaps.write('.'))
//.pipe(rename('app.min.js'))
//.pipe(gulp.dest('assets/dist/js'))  ;
//});

function minifyGoogleMaps() {
	gulp.src('assets/js/map.js')
		.pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(sourcemaps.write('.'))
		.pipe(rename('map.min.js'))
        .pipe(gulp.dest('assets/dist/js'))
        .pipe(browsersync.stream());
};

function runSass() {
    return gulp
        .src([stylesIn, faStylesIn, materialStylesIn])
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(stylesOut))
        .pipe(livereload());
};

//gulp.task('concatCSS', gulp.series('sass', function () {
function concatCSS() {
	gulp.src(["assets/css/slick.css",
		"assets/css/slick-theme.css",
		"assets/css/materialize.css",
		"assets/css/styles.css",
		"assets/css/font-awesome.css"])
		.pipe(minifyCSS())
		.pipe(concat('styles.min.css'))
		.pipe(gulp.dest('assets/css'));
};

// Watch
gulp.task('watch', gulp.parallel(function () {
	livereload.listen();

	return gulp
		// watch the input for change and run our tasks when something happens
		.watch([stylesIn, faStylesIn, materialStylesIn], ['sass'])
		// when there is a change,
		// log a message in the console
		.on('change', function (event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		});
}));


gulp.task('watchScripts', gulp.parallel(function () {
	gulp.watch(['assets/js/*.js', '!assets/js/donorList.js', '!assets/dist/js/app.js', '!assets/dist/js/app.min.js', '!assets/dist/js/donorList.js'], ['concatScripts', 'minifyGoogleMaps']); //if we don't ignore the output files the watch command will see the change and go on an endless loop!
	console.log('Gulp is watching for script changes!');
}));

// Default task
//gulp.task('default', ['watch', 'watchScripts', 'concatCSS']);
gulp.task('default', gulp.parallel('watch','watchScripts', concatCSS))

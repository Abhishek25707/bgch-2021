(function ($) {
	$.fn.popup = function (options) {
		var settings = $.extend({
			htmlURL: "",
			html: "",
			container: '<div id="popupOverlay" class="sitePopup"><div class="sitePopup_container"><div id="popupCloseBtn" class="sitePopup_closeBtn"></div></div></div>'
		}, options);

		return this.on('click', function () {
			//open the image popup from the template file
			if (htmlURL === "") {
				$.ajax({
					url: settings.htmlURL,
					cache: false
				})
					.done(function (html) {
						$('.sitePopup_container').append(html);
					});
			}

			else {
				$('.sitePopup_container').html(html);
			}
			$('body').prepend(settings.container);
			//Add the listener for the close button
			$('body').on('click', '#popupCloseBtn', function () {
				$('#popupOverlay').remove();
				$('html').removeClass('sitePopupOpen');
			});

			//add the class to the html tag so we can apply the proper classes to the body while the popup is open
			$('html').addClass('sitePopupOpen');
		});
	};
}(jQuery));

// example
// $( "#openPopup" ).popup({htmlURL: "example.html"});
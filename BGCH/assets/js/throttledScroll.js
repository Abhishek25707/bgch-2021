function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

var navToggle = debounce(function() {
	  //nav class toggle onscroll
    // get the value of the bottom of the #main element by adding the offset of that element plus its height, set it as a variable
    var siteHeader = $('#m-siteHeader');
        // Round here to reduce a little workload
        var scroll = Math.round($('.allowScrollParallax').scrollTop());
				if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)  {
					scroll = Math.round($(window).scrollTop());
				}
        if (scroll > 10) {
            siteHeader.addClass('past-main');
        } else {
            siteHeader.removeClass('past-main');
        }

}, 10);

var allowScrollParallax = document.getElementById('allowScrollParallax');
var ua = window.navigator.userAgent;
var msie = ua.indexOf("MSIE ");
allowScrollParallax.addEventListener('scroll', navToggle);

if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)  {
	window.addEventListener('scroll', navToggle);
}

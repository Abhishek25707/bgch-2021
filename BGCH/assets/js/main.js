var removeHash = function () {
	window.history.pushState("", document.title, window.location.pathname);
};

var aurl = window.location.href; // Get the absolute url
var ua = window.navigator.userAgent;
var msie = ua.indexOf("MSIE ");

function grabCursor() { // THIS GUY MAKES THE CURSOR GRABBY
	if (window.innerHeight > window.innerWidth || $('html').hasClass('touch')) {
		$(".no_touch_mode").removeClass("no_touch_mode");
		$(".split-right, .split-left").addClass("touch_mode_grab");
		$(".split-right, .split-left").mousedown(function () {
			$(this).removeClass("touch_mode_grab")
				.addClass("touch_mode_grabbing");
		}).mouseup(function () {
			$(this).removeClass("touch_mode_grabbing")
				.addClass("touch_mode_grab");
		});
	} else {
		$(".touch_mode_grabbing").removeClass("touch_mode_grabbing");
		$(".touch_mode_grab").removeClass("touch_mode_grab");
		$(".split-right, .split-left").addClass("no_touch_mode");
	}
	return this;
}
// function setBodyHeight() {
//     $("html, body").css({
//   	height: $(window).height()
//   	});
// };
var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad|MacIntel|Macintosh|MacPPC|Mac68K)/i) ? true : false;
$(document).ready(function () {
	if (mac) {
		$('#m-siteHeader').css({ "width": "100%", "max-width": "100%" });
	};

	///clear email signup placeholder on click

	$('#emailInput').on('click', function () {
		$(this).removeAttr('placeholder');
	});

	/////////////////////////////////////////
	///////////////////////CopyTextAreas

	var $copyTextareaBtn = $('.copyButton');

	$copyTextareaBtn.on('click', function (event) {
		var copyTextarea = $(this).parent().next($('textarea.email'));
		copyTextarea.select();

		var successful = document.execCommand('copy');
		var msg = successful ? 'successful' : 'unsuccessful';
		$(this).next($('.tooltip')).html('Copied!');

		$(this).on('mouseleave', function () {
			$(this).next($('.tooltip')).html('Copy to clipboard');
		});
	});

	/////////////////////////////////////

	if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1) {
		$('.allowScrollParallax, .parallax-image').addClass('removeParallax');
		var cnt = $(".allowScrollParallax").contents();
		$(".allowScrollParallax").replaceWith(cnt);
		$('#m-siteHeader').css({ 'width': '100%', 'max-width': '100%' });
	}

	////////////////////////////////////////////////
	// Slick Init's

	var $slickElement = $('.slick-slides');

	$('.learn1').hover(function () {
		$(".nav-ul li a:eq( 3 )").toggleClass('active');
	})
	$('.learn2').hover(function () {
		$(".nav-ul li a:eq( 4 )").toggleClass('active');
	})
	$('.learn3').hover(function () {
		$(".nav-ul li a:eq( 5 )").toggleClass('active');
	})
	$('.learn4').hover(function () {
		$(".nav-ul li a:eq( 6 )").toggleClass('active');
	})

	$slickElement.slick({
		//setting-name: setting-value
		adaptiveHeight: true,
		//arrows: false,
		padding: '0px',
		// mobileFirst: true,
		//adaptiveHeight: true,
		infinite: true,
		slidesToShow: 1,
		lazyLoad: 'ondemand',
		speed: 500
		//fade: true
	});

	if (window.innerWidth > 1023) {
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
			$('.single-item').slick({
				dots: true,
				slidesToShow: 1,
				lazyLoad: 'ondemand',
				fade: true,
				adaptiveHeight: true,
				centerMode: false
			});
		} else {
			$('.single-item').slick({
				dots: true,
				slidesToShow: 1,
				lazyLoad: 'ondemand',
				centerMode: true,
				centerPadding: '20%'
			});
		}
	} else {
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
			$('.single-item').slick({
				dots: true,
				slidesToShow: 1,
				lazyLoad: 'ondemand',
				fade: true,
				centerMode: false
			});
		} else {
			$('.single-item').slick({
				dots: true,
				slidesToShow: 1,
				lazyLoad: 'ondemand',
				centerMode: true,
				centerPadding: '10%'
			});
		}
	}

	//slick init's end
	////////////////////////////////////////////////

	//active nav classes based on url

	$('.nav-ul a').filter(function () {
		return $(this).prop('href') === aurl;
	}).addClass('active');

	// if (aurl == window.location.origin + '/' || aurl == window.location.origin + '/Home/Index') {
	//   $('.m-siteNav .scrollToTop').addClass('active');
	// }
	//active nav classes based on url

	/*
 ////////////////////////////////////////////////
// Donation form logic

//Make sure everything is set to starting state if the user navigates back from paypal
	$('#donate-area, #donate-background').removeClass('open');
	$('body').removeClass('preventScroll');
	$('.btn-recur-input').removeClass('shrink');
	$('#recurringInput').removeClass('grow');
	$('#checkbox-1').prop('checked', false);
	$('#recurringInput').val('');

//get and set the code to send to paypal in order to correctly redirect user based on button choice
	$('.form-btn').click(function(e){
		//var form = $(this).parents('form:first');
		$('#hostedValue').val($(this).data('hostedvalue'));
		//form.submit();
	});

// prevent submit on first click to allow the input to show, allow submit on second click after user has chosen amount
	var userChoiceCount = 1;
	$('.btn-recur-input').click(function(e){
			if(userChoiceCount == 1){
				e.preventDefault();
				$(this).addClass('shrink');
				$('#recurringInput').addClass('grow');
				userChoiceCount = 2;
			} else {
				$(this).click(function(){
					$('#userChoice').submit();
				})
			}
	});

//validate input for currency
	$("#recurringInput").on("keyup", function(){
			var valid = /^\d{0,6}(\.\d{0,2})?$/.test(this.value),
					val = this.value;

			if(!valid){
					this.value = val.substring(0, val.length - 1);
			}
	});

//hide or show button sets that send user to one time or recurring payments
$('#checkbox-1').change(
	function(){
			if (this.checked) {
					$('.recurring, .userChoice').removeClass('hide-me');
					$('.non-recurring').addClass('hide-me');
			} else {
					$('.recurring, .userChoice').addClass('hide-me');
					$('.non-recurring').removeClass('hide-me');
					$('.btn-recur-input').removeClass('shrink');
					$('#recurringInput').removeClass('grow');
					$('#recurringInput').val('');
					userChoiceCount = 1;
			}
	});

$('.donate-button, .donate').on("click", function(){
	$('#donate-area, #donate-background').addClass('open');
	$('body').addClass('preventScroll');
	$('#recurringInput').val('');
});

$('.recurringDonate').on("click", function(){
	$('#donate-area, #donate-background').addClass('open');
	$('body').addClass('preventScroll');
	$('#recurringInput').val('');
	$('#checkbox-1').prop('checked', true);
	$('.recurring, .userChoice').removeClass('hide-me');
	$('.non-recurring').addClass('hide-me');
});

$('.exit-donate, #donate-background').on("click", function(){
	//reset everything on exit
	resetModal();
});

function resetModal() {
	$('#donate-area, #donate-background').removeClass('open');
	$('body').removeClass('preventScroll');
	$('.btn-recur-input').removeClass('shrink');
	$('#recurringInput').removeClass('grow');
	userChoiceCount = 1;
	$('#checkbox-1').prop('checked', false);
	$('#recurringInput').val('');
	$('.non-recurring.non-GFD, .cbp-mc-column.non-GFD, .H').removeClass('hide-me');
	$('.GFD, .recurring, .userChoice, .H').addClass('hide-me');
};

	$('#GFD-donate-button').on("click", function(){
		$('.non-recurring.non-GFD, .cbp-mc-column.non-GFD, .H').addClass('hide-me');
		$('.GFD').removeClass('hide-me');
		$('#donate-area, #donate-background').addClass('open');
		$('body').addClass('preventScroll');
	});

	$('#LB-donate-button').on("click", function () {
		$('.non-recurring.non-GFD, .cbp-mc-column.non-GFD, .H').addClass('hide-me');
		$('.LB').removeClass('hide-me');
		$('#donate-area, #donate-background').addClass('open');
		$('body').addClass('preventScroll');
	});

	$('#H-donate-button').on("click", function () {
		$('.non-recurring.non-GFD, .cbp-mc-column.non-GFD, .H').addClass('hide-me');
		$('.H').removeClass('hide-me');
		$('#donate-area, #donate-background').addClass('open');
		$('body').addClass('preventScroll');
	});

// Donation form logic end
/////////////////////////////////////////////////
*/

	//prevent scroll when mobile nav is open
	$('.priority-nav__dropdown-toggle').on("click", function () {
		if ($(this).hasClass('is-open')) {
			$('body').addClass('preventScroll');
		} else {
			$('body').removeClass('preventScroll');
		}
	});

	//hide reveal toggle controls
	////////////////////////////////////////

	///////////////////////////////////////////

	var $toggleParent = $('.toggle').parent();

	$('.toggle').click(function (e) {
		e.stopPropagation();
		$(this).toggleClass('fa-chevron-down fa-minus');
		$(this).parent().next('.flex-container').toggleClass('reveal');
	});

	$('.toggleParent').click(function (e) {
		$(this).children('.toggle').trigger('click');
	});

	// toggle for leadership bios

	$('.leadershipTeam .flex-item').click(function () {
		$(this).toggleClass('reveal');
		var data = $(this).data("person");
		$('.leaderBio').removeClass('reveal');
		$('.fa').addClass('fa-plus-circle').removeClass('fa-minus-circle');


		if ($(this).hasClass('reveal')) {
			$(this).siblings().removeClass('reveal');
			$('.leaderBio.' + data).addClass('reveal');
			$('.fa.' + data).toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');

		}
	});

	//$('.flex-item-inner.kevin').click(function (e) {
	//	if ($(window).width() > 500) {
	//		$('.leaderBio.zeba, .leaderBio.carlos, .leaderBio.susy').removeClass('reveal');
	//	}
	//	$('.leaderBio.kevin').toggleClass('reveal');
	//	$('.fa.zeba, .fa.carlos, .fa.susy').addClass('fa-plus-circle').removeClass('fa-minus-circle');
	//	$('.fa.kevin').toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');
	//});

	//$('.flex-item-inner.zeba').click(function (e) {
	//	if ($(window).width() > 500) {
	//		$('.leaderBio.kevin, .leaderBio.carlos, .leaderBio.susy').removeClass('reveal');
	//	}
	//	$('.leaderBio.zeba').toggleClass('reveal');
	//	$('.fa.kevin, .fa.carlos, .fa.susy').addClass('fa-plus-circle').removeClass('fa-minus-circle');
	//	$('.fa.zeba').toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');
	//});

	//$('.flex-item-inner.carlos').click(function (e) {
	//	if ($(window).width() > 500) {
	//		$('.leaderBio.kevin, .leaderBio.zeba, .leaderBio.susy').removeClass('reveal');
	//	}
	//	$('.leaderBio.carlos').toggleClass('reveal');
	//	$('.fa.kevin, .fa.zeba, .fa.susy').addClass('fa-plus-circle').removeClass('fa-minus-circle');
	//	$('.fa.carlos').toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');
	//});

	//$('.flex-item-inner.susy').click(function (e) {
	//	console.log('susy was clicked');
	//	if ($(window).width() > 500) {
	//		$('.leaderBio.kevin, .leaderBio.zeba, .leaderBio.carlos').removeClass('reveal');
	//	}
	//	$('.leaderBio.susy').toggleClass('reveal');
	//	$('.fa.kevin, .fa.zeba, .fa.carlos').addClass('fa-plus-circle').removeClass('fa-minus-circle');
	//	$('.fa.susy').toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');
	//});

	//end hide reveal toggle controls
	////////////////////////////////////////

	//active nav classes based on url

	$('.nav-ul a').filter(function () {
		return $(this).prop('href') === aurl;
	}).addClass('active');

	grabCursor();
});

// function recreateWall() {
//   $('#wall').empty();
//   $('#wall').append('<div id="social-stream"></div>');
//   if ($(window).width() > 736) {
//     $('#social-stream').removeClass('classic','light');
//     $('#desktopWallStyles').remove();
//     $('#desktopWallScript').remove();
//     $('#mobileWallStyles').remove();
//     desktopSocial();
//     } else {
//       $('#desktopWallStyles').remove();
//       $('#desktopWallScript').remove();
//       $('#mobileWallStyles').remove();
//       mobileSocial();
//       }
// };

$(window).smartresize(function () {
	grabCursor();
});

// $(window).on("orientationchange",function(){
//   if ($(window).width() < 736) {
//     setBodyHeight();
//     };
// });

// $(window).load(function() {
//     setTimeout(function(){removeHash();}, 500);
// });
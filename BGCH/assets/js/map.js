var styledMapType = new window.google.maps.StyledMapType([
  {
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#f5f5f5"
        }
      ]
  },
  {
      "elementType": "labels.icon",
      "stylers": [
        {
            "visibility": "off"
        }
      ]
  },
  {
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#616161"
        }
      ]
  },
  {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
            "color": "#f5f5f5"
        }
      ]
  },
  {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
            "visibility": "off"
        }
      ]
  },
  {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#bdbdbd"
        }
      ]
  },
  {
      "featureType": "administrative.neighborhood",
      "stylers": [
        {
            "visibility": "off"
        }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#eeeeee"
        }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "labels.text",
      "stylers": [
        {
            "visibility": "off"
        }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#757575"
        }
      ]
  },
  {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#e5e5e5"
        }
      ]
  },
  {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#9e9e9e"
        }
      ]
  },
  {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#ffffff"
        }
      ]
  },
  {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [
        {
            "visibility": "off"
        }
      ]
  },
  {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#757575"
        }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#dadada"
        }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#616161"
        }
      ]
  },
  {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#9e9e9e"
        }
      ]
  },
  {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#e5e5e5"
        }
      ]
  },
  {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#eeeeee"
        }
      ]
  },
  {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
            "color": "#c9c9c9"
        }
      ]
  },
  {
      "featureType": "water",
      "elementType": "labels.text",
      "stylers": [
        {
            "visibility": "off"
        }
      ]
  },
  {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
            "color": "#9e9e9e"
        }
      ]
  }
], { name: 'Styled Map' });

var locations = [
    [
    "<a class='mapUrl' href='//goo.gl/maps/B3nQsnjvxPQ2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Fort Bend Club</p>",
     "5525 Hobby Street, Houston, TX 77053",
    "29.585397",
    "-95.472738"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/4ey8yuG44gv' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Jim & Barbara Morefield Club</p>",
    "5950 Selinsky Rd, Houston, TX 77048",
    "29.636724",
    "-95.325350"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/1arK6YQAReM2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>John & Cissy Havard Club</p>",
    "1520 Airline Dr., Houston, TX 77009",
    "29.799493",
    "-95.381472"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/P3wyhsG7Fuo' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Spring Branch Club</p>",
    "8575 Pitner Rd., Houston, TX 77080",
    "29.828438",
    "-95.501619"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/wdb5BL1ubSB2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Stafford Club</p>",
    "3110 5th St., Stafford, Texas 77477",
    "29.599103",
    "-95.555778"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/9YzqGjo1tP92' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Johnny Mitchell Club</p>",
    "4420 Avenue P, Galveston, TX 77550",
    "29.285830",
    "-94.814396"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/usUTUYZsF7C2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Richmond-Rosenberg Club</p>",
    "1800 James L. Pink Blvd, Richmond, TX",
    "29.589744",
    "-95.774096"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/q24FDcfH87VL8kyR6' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Royal ISD Club</p>",
    "2500 Durkin Rd, Brookshire, TX 77423",
    "29.806991",
    "-95.967893"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/1arK6YQAReM2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Houston Texans Teen Club</p>",
    "1520 Airline Dr., Houston, TX 77009",
    "29.799493",
    "-95.381472"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/6aXA6BGF1cB2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Allen Parkway Club</p>",
    "815 Crosby St., Houston, Texas 77019",
    "29.759059",
    "-95.375842"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/ckMoDyphEot' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Housman Club</p>",
    "6705 Housman, Houston, Texas 77005",
    "29.800315",
    "-95.469791"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/tBzAPJ3KzkF2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Buffalo Creek Club</p>",
    "2801 Blalock Rd., Houston, Texas 77080",
    "29.819682",
    "-95.523967"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/hZpDEGPsaWU2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>KIPP Sunnyside Club</p>",
    "11000 Scott, Houston Texas 77047",
    "29.637775",
    "-95.370522"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/z1wN3ApeKsx' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Boys and Girls Club Houston, HQ</p>",
    "815 Crosby St., Houston, Texas 77019",
    "29.759059",
    "-95.375842"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/zpfb8ZsSTNT2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Wharton Dobson Club & Teen Scene Club</p>",
    "2120 Newton St., Wharton, Texas 77488",
    "29.329547",
    "-96.091216"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/LQFprHGCHDn' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Spring Branch Elementary Club</p>",
    "1700 Campbell Rd., Houston, Texas 77080",
    "29.801756",
    "-95.515850"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/3f2LamGmoyx' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Spring Oaks Middle Club</p>",
    "2150 Shadowdale Dr., Houston, Texas 77043",
    "29.813534",
    "-95.552620"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/uXPAWrdqVY62' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>The Women's Home Club</p>",
    "1839 Jacquelyn Rd., Houston, Texas 77055",
    "29.804855",
    "-95.481299"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/zNWZtew4M6H2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Westwood Elementary Club</p>",
    "10595 Hammerly Blvd., Houston, Texas 77043",
    "29.811054",
    "-95.557609"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/bfHgpeaWsmt' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Woodview Elementary Club</p>",
    "9749 Cedardale Dr., Houston, Texas 77055",
    "29.790759",
    "-95.532736"
    ],
    [
    "<a class='mapUrl' href='//goo.gl/maps/d3AgfXoKW4N2' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Kashmere Gardens/North Forest Club</p>",
    "4802 Lockwood Dr., Houston, Texas 77026",
    "29.804393",
    "-95.316126"
    ],
    [
        "<a class='mapUrl' href='//goo.gl/maps/pDfcJXJWdt8VdDxC7' target='_blank'><div class='mapContent'><img class='mapIcon' src='/assets/images/logoNaked.png'/><p>Mission Bend Club</p>",
        "8709 Addicks Clodine Rd, Houston, TX 77083",
        "29.68237",
        "-95.66176"
    ]
];

gmarkers = [];

var map, location, myLatLng, marker, bounds;
var bounds = new google.maps.LatLngBounds();

function initialize() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        gestureHandling: 'cooperative',
        scrollwheel: false,
        center: new google.maps.LatLng(29.760427, -95.369803),
        mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'hybrid', 'terrain',
                    'styled_map']
        }
    })


    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    var infowindow = new google.maps.InfoWindow();


    function createMarker(latlng, html) {
        var marker = new google.maps.Marker({
            position: latlng,
            icon: '//maps.google.com/mapfiles/ms/icons/blue.png',
            map: map
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(html);
            infowindow.open(map, marker);
        });
        return marker;
    }

    for (var i = 0; i < locations.length; i++) {
        myLatLng = new google.maps.LatLng(locations[i][2], locations[i][3]);
        gmarkers.push(
            createMarker(new google.maps.LatLng(
              locations[i][2],
              locations[i][3]),
              locations[i][0] + "<p>" + locations[i][1] + "</p></div></a>")
        );
        marker = new google.maps.Marker({
            position: myLatLng
        });
        bounds.extend(marker.getPosition());
    }

    map.fitBounds(bounds);
    var listener = google.maps.event.addListener(map, "idle", function () {
        if (map.getZoom() < 8) map.setZoom(8);
        google.maps.event.removeListener(listener);
    });

};


google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, "resize", function () {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
});

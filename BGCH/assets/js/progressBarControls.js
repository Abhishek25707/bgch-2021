var bar = new ProgressBar.Circle(graduation, {
  color: '#000',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 4,
  trailWidth: 4,
  easing: 'easeInOut',
  duration: 1400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#8ac53e', width: 4 },
  to: { color: '#8ac53e', width: 4 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = Math.round(circle.value() * 100);
    if (value === 0) {
      circle.setText('%');
    } else {
      circle.setText(value + '%');
    }

  }
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

var bar2 = new ProgressBar.Circle(grades, {
  color: '#000',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 4,
  trailWidth: 4,
  easing: 'easeInOut',
  duration: 1400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#8ac53e', width: 4 },
  to: { color: '#8ac53e', width: 4 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = Math.round(circle.value() * 100);
    if (value === 0) {
      circle.setText('%');
    } else {
      circle.setText(value + '%');
    }

  }
});
bar2.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar2.text.style.fontSize = '2rem';

var bar3 = new ProgressBar.Circle(homework, {
  color: '#000',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 4,
  trailWidth: 4,
  easing: 'easeInOut',
  duration: 1400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#8ac53e', width: 4 },
  to: { color: '#8ac53e', width: 4 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = Math.round(circle.value() * 100);
    if (value === 0) {
      circle.setText('%');
    } else {
      circle.setText(value + '%');
    }

  }
});
bar3.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar3.text.style.fontSize = '2rem';

//trigger animation as each individual bar comes into view (this will come into play more apparently in single column mobile view)

var $window = $('#allowScrollParallax');
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)  {
  $window = $(window);
}
var $bar1 = $('#graduation');
var $bar2 = $('#grades');
var $bar3 = $('#homework');

function check_if_in_view() {
    var window_top_position;
    var window_height;

    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
       window_height = $(window).height();
       window_top_position = $(window).scrollTop();
    } else {
       window_height = $('#allowScrollParallax').height();
       window_top_position = $('#allowScrollParallax').scrollTop();
    }

    var window_bottom_position = (window_top_position + window_height);

    $.each($bar1, function() {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);

      //check to see if this current container is within viewport
      if ((element_bottom_position >= window_top_position) &&
          (element_top_position <= window_bottom_position)) {
            bar.animate(0.83);
      }
  });

    $.each($bar2, function() {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);

      //check to see if this current container is within viewport
      if ((element_bottom_position >= window_top_position) &&
          (element_top_position <= window_bottom_position)) {
            if ($(window).width() > 767) {
              setTimeout(function(){
                bar2.animate(0.89);
              }, 400);
                } else {
                  bar2.animate(0.89);
                }
      }
  });

    $.each($bar3, function() {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);

      //check to see if this current container is within viewport
      if ((element_bottom_position >= window_top_position) &&
          (element_top_position <= window_bottom_position)) {
            if ($(window).width() > 767) {
              setTimeout(function(){
                bar3.animate(0.67);
              }, 800);
                } else {
                  bar3.animate(0.67);
                }
      }
  });

}



$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');

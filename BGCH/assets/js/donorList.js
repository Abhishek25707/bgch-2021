new window.Vue({
    el: '#app',
    data: {
        corporateDonors1: [
            {
                name: "American Red Cross",
                url: "https://www.redcross.org/"
            },
            {
                name: "Fort Bend County",
                url: "https://www.fortbendcountytx.gov/"
            },
            {
                name: "The George Foundation",
                url: "http://www.thegeorgefoundation.org/"
            },
            {
                name: "The Henderson-Wessendorff Foundation",
                url: "https://hw-foundation.com/"
            },

            {
                name: "The Houston Texans Foundation",
                url: "https://www.houstontexans.com/community/"
            },

            {
                name: "Mary Louise Dobson Foundation",
                url: "https://www.marylouisedobsonfoundation.org/"
            },

            {
                name: "Mas Restaurant Group/Taco Bell",
                url: "http://masrestaurantgroup.info/"
            },

            {
                name: "The Moody Foundation",
                url: "https://moodyf.org/"
            },
            {
                name: "Phillips 66",
                url: "https://www.phillips66.com/"
            },
            {
                name: "ReBuild Texas",
                url: "https://www.rebuildtx.org/"
            },
            {
                name: "Sysco Corporation",
                url: "https://www.sysco.com/"
            },
            {
                name: "Texas Center for Child & Family Services",
                url: "https://tacfs.org/"
            },
            {
                name: "Texas Children’s Health Plan",
                url: "https://www.texaschildrenshealthplan.org/"
            },
            {
                name: "The State of Texas",
                url: "https://www.texas.gov/"
            },
            {
                name: "Sean Wheeler",
                url: "#"
            },
            {
                name: "United Way of Greater Houston",
                url: "https://www.unitedwayhouston.org/"
            },
            {
                name: "Office of Juvenile Justice and Delinquency Prevention",
                url: "https://ojjdp.ojp.gov/"
            }

            
        ],
        corporateDonors2: [
            {
                //name: "Office of Juvenile Justice and Delinquency Prevention",
                //url: "https://ojjdp.ojp.gov/"
            }
        ],
        corporateDonors3: [
            {
                name: "Houston Texans",
                url: "http://www.houstontexans.com/community/index.html"
            },
            {
                name: "Phillips 66",
                url: "http://www.phillips66.com/"
            }
        ],
        corporateDonors4: [
            {
                name: "Best Buy Foundation",
                url: "https://corporate.bestbuy.com/social-impact/"
            },
            {
                name: "The Clubhouse Network",
                url: "https://theclubhousenetwork.org/"
            }
        ],
        corporateDonors5: [
            {
                name: "Holthouse Foundation for Kids",
                url: "http://www.hffk.org/"
            },
            {
                name: "Sysco",
                url: "https://www.sysco.com/"
            }
        ],
        corporateDonors6: [
            {
                name: "Texas Center for Child and Family Studies (The Center)",
                url: "https://www.tacfs.org/thecenter"
            },
            {
                name: "Texas Education Agency",
                url: "https://tea.texas.gov/"
            }
        ],
        corporateDonors7: [
            {
                name: "Bridge-Up at Menninger - The Menninger Clinic",
                url: "https://www.menningerclinic.com/about/community-engagement/bridgeup-at-menninger"
            },
            {
                name: "Chevron Corporation",
                url: "https://www.chevron.com/"
            },
            {
                name: "ConocoPhillips",
                url: "http://www.conocophillips.com/communities/"
            },
            {
                name: "DPR Foundation",
                url: "https://www.dpr.com/company/community"
            },
            {
                name: "Fondren Foundation",
                url: ""
            },
            {
                name: "Houston Endowment, Inc",
                url: "https://www.houstonendowment.org/"
            },
            {
                name: "Ippolito Charitable Foundation",
                url: "http://ippolitofoundation.org/"
            },
            {
                name: "Major League Baseball",
                url: "https://www.mlb.com/"
            }
        ],
        corporateDonors8: [
            {
                name: "M.D. Anderson Foundation",
                url: "https://www.mdanderson.org/"
            },
            {
                name: "Moody Methodist Church Permanent Endowment Fund",
                url: "http://www.moody.org/PEF"
            },
            {
                name: "National Oilwell Varco",
                url: "https://www.nov.com/"
            },
            {
                name: "Ross Stores",
                url: "https://www.rossstores.com/"
            },
            {
                name: "Texas Children's Hospital",
                url: "https://www.texaschildrens.org/"
            },
            {
                name: "Transocean",
                url: "https://www.deepwater.com/"
            },
            {
                name: "United Way/Out2Learn",
                url: "http://out2learnhou.org/"
            },
            {
                name: "The Women's Home",
                url: "https://www.thewomenshome.org/"
            }
        ],
        communityPartners1: [
          {
          name: "All Kids Alliance",
          url: "http://www.allkidsalliance.org/default.aspx"
          },
          {
          name: "American Red Cross",
          url: "http://www.redcross.org/local/texas/gulf-coast/local-chapters/houston"
          },
          {
          name: "Be A Champion",
          url: "http://www.bachamp.org/"
          },
          {
          name: "Boy Scouts of America",
          url: "https://www.scouting.org/"
          },
          {
          name: "Child Care Council of Greater Houston",
          url: "http://www.cccghi.com/"
          },
          {
          name: "City of Houston Housing and Community Development Department",
          url: "https://www.houstontx.gov/housing/"
          },
          {
          name: "Communities in Schools",
          url: "http://cishouston.org/"
          },
          {
          name: "Comp-U-Dopt",
          url: "http://compudopt.org/"
          },
          {
          name: "Family Houston",
          url: "https://www.familyhouston.org/"
          },
          {
          name: "Fort Bend County",
          url: "https://www.fortbendcountytx.gov/"
          },
          {
          name: "Galveston Police Department",
          url: "http://www.galvestontx.gov/165/Police-Department"
          }          
        ],
        communityPartners2: [
          {
          name: "Harris County Department of Education – CASE for Kids",
          url: "http://www.hcde-texas.org/who-we-are/divisions-and-leadership/case-for-kids/"
          },
          {
          name: "Harris County Public Library",
          url: "http://www.hcpl.net/"
          },
          {
          name: "Houston City Council",
          url: "https://www.houstontx.gov/council/"
          },
          {
          name: "Houston Food Bank",
          url: "http://www.houstonfoodbank.org/"
          },
          {
          name: "Houston Police Department",
          url: "http://www.houstontx.gov/police/"
          },
          {
          name: "Lemonade Day",
          url: "https://lemonadeday.org/houston"
          },
          {
          name: "NASA Johnson Space Center",
          url: "https://spacecenter.org/"
          },
          {
          name: "Spring Branch Family Development Center",
          url: "http://www.sbfdc.org/wp/"
          },
          {
          name: "SpringSpirit",
          url: "http://www.springspiritbaseball.org/"
          },
          {
          name: "Texas Children's Health Plan",
          url: "http://www.texaschildrenshealthplan.org/"
          },
          {
          name: "United Way of Galveston",
          url: "https://uwgalv.org/"
          },
          {
          name: "United Way of Greater Houston",
          url: "https://www.unitedwayhouston.org/"
          }
        ],
        milbankDonors1: [{
                name: "Lesley and Gerald Bodzy"
            },
            {
                name: "Maria and Pedro Caruso"
            },
            {
                name: "Pam and Keith Fullenweider"
            },
            {
                name: "Joey and Bill Goetz"
            },
            {
                name: "Maureen and John Graf"
            },
            {
                name: "Teryl and Bob Herman"
            },
            {
                name: "Lisa and Michael Holthouse"
            },
            {
                name: "Danielle Hunter"
            },
            {
                name: "Ronnie Isenberg"
            },
            {
                name: "Carolyn and Matt Khourie"
            },
            {
                name: "Lynette and Mike Kuznar"
            },
            {
                name: "Laura and Will Leven"
            },
            {
                name: "Gina and Carl Luna"
            },
            {
                name: "Peggy and Pete Miller"
            }
        ],
        milbankDonors2: [
            {
                name: "Sarah Morgan and Oscar Brown"
            },
            {
                name: "Kristin and Barry Palmer"
            },
            {
                name: "Chris Papouras"
            },
            {
                name: "Waverly and Adam Peakes"
            },
            {
                name: "Michelle and Rick Perez"
            },
            {
                name: "Jackie and Al Richey"
            },
            {
                name: "Cynthia Sanford"
            },
            {
                name: "Anita Sehgal"
            },
            {
                name: "Kathleen and Michael Terracina"
            },
            {
                name: "Melinda and Jeremy Thigpen"
            },
            {
                name: "Beth and Schuyler Tilney"
            },
            {
                name: "Katharine and Bill Van Wie"
            },
            {
                name: "Callie and Sean Wheeler"
            }
        ],
        lifetimeDonors1: [{
                name: "Pam and Keith Fullenweider"
            },
            {
                name: "Maureen & John Graf"
            },
            {
                name: "Laura and William Leven"
            },
            {
                name: "Gina and Carl Luna"
            },
            {
                name: "Patti and Mike Morgan"
            },
            {
                name: "Christopher Papouras"
            }
        ],
        lifetimeDonors2: [{
                name: "Waverly and Adam Peakes"
            },
            {
                name: "Jackie and Al Richey"
            },
            {
                name: "Cynthia Sanford"
            },
            {
                name: "Katharine and Bill Van Wie"
            },
            {
                name: "Tracy and David Wegner"
            }
        ]
    }
})

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BGCH
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add(
                new Route(
                    "Safety-And-Security/{action}/{id}",
                    new RouteValueDictionary(new { controller = "SafetyAndSecurity", action = "Index", id = "" }),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "About-Us/{action}/{id}",
                    new RouteValueDictionary(new { controller = "AboutUs", action = "Index", id = "" }),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "Our-Impact/{action}/{id}",
                    new RouteValueDictionary(new { controller = "OurImpact", action = "Index", id = "" }),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "Donors-And-Partners/{action}/{id}",
                    new RouteValueDictionary(new { controller = "DonorsAndPartners", action = "Index", id = "" }),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "Events/{action}/{id}",
                    new RouteValueDictionary(new { controller = "Events", action = "Index", id = "" }),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "News-And-Media/{action}/{id}",
                    new RouteValueDictionary(new { controller = "NewsAndMedia", action = "Index", id = "" }),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "{action}",
                    new RouteValueDictionary(new {controller = "Home", action = "Index"}),
                    new RouteValueDictionary(new {IsRootAction = new IsRootActionConstraint()}),
                    new HyphenatedRouteHandler()));

            routes.Add(
                new Route(
                    "{controller}/{action}/{id}",
                    new RouteValueDictionary(new {controller = "Home", action = "Index", id = ""}),
                    new HyphenatedRouteHandler()));
        }
    }

    public class IsRootActionConstraint : IRouteConstraint
  {
    private Dictionary<string, Type> _controllers;

    public IsRootActionConstraint()
    {
      _controllers = Assembly
                      .GetCallingAssembly()
                      .GetTypes()
                      .Where(type => type.IsSubclassOf(typeof(Controller)))
                      .ToDictionary(key => key.Name.Replace("Controller", "").ToLower().Replace("_", "-"));
    }

    public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
    {
      var action = values["action"] as string;
      var isMatch = !_controllers.Keys.Contains(action.ToLower());
      return isMatch;
    }
  }
}

﻿using System.Web.Mvc;

namespace BGCH.Controllers
{
    public class OurImpactController : Controller
    {
        //Subpages for "Our Impact" header
        public ActionResult Parent_Resources() { return View(); }
        public ActionResult Registration() { return View(); }
        public ActionResult High_Quality_Club_Experience() { return View(); }
        public ActionResult Youth_Of_The_Year() { return View(); }
        
    }
}
﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using BGCH.Models;

namespace BGCH.Controllers
{
    public class DonorsAndPartnersController : Controller
    {
		//Subpages for "Donors & Partners" header

		public ActionResult Annual_Reporting() { return View(); }
		public ActionResult Great_Futures_Society() { return View(); }
		public ActionResult Nineteen_Fifty_Two_Legacy_Society() { return View(); }
		public ActionResult Donate() { return View(); }
		public ActionResult Champions_Of_Caring() { return View(); }
		public ActionResult Invest_In_Youth_Today() { return View(); }
        public ActionResult Volunteers() { return View(); }
		public ActionResult Club_Blue() { return View(); }



		[HttpGet]
		public ActionResult In_Kind_Donations()
		{
			return View(new InKindDonationForm());
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult In_Kind_Donations(InKindDonationForm model)
		{
			if (ModelState.IsValid)
			{
				var email = new InKindDonationEmail
				{
					FormElements = new Dictionary<string, string>{
						{"Donated Items", model.DonatedItems!=null?string.Join(", ", model.DonatedItems) :string.Empty},
						{"Estimated Fair Market Value", model.EstimatedFairMarketValue},
						{"Method used to determine efmv", model.MethodUsedToEstimate},
						{"Date of Donation", model.DateOfDonation?.ToShortDateString()},
						{"Donor Name", model.DonorName},
						{"Contact and Title", model.ContactAndTitle},
						{"Phone Number", model.Phone},
						{"Email Address", model.Email},
						{"Special Instructions", model.SpecialInstructions},
						{"Street Address", model.StreetAddress},
						{"City", model.City},
						{"State", model.State},
						{"ZIP Code", model.Zip},
						{"I have other items I'd like to donate", model.OtherItemsToDonate}
					},
					To = ConfigurationManager.AppSettings.Get("Email.InKindDonation.To")
				};
                email.Send();
				return PartialView("_InKindThankYou");
			}
            return View(model);
		}

		[HttpGet]
		public ActionResult Young_Professionals()
		{
			return View(new YoungProfessionalsForm());
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Young_Professionals(YoungProfessionalsForm model)
		{
			if (ModelState.IsValid)
			{
				var email = new YoungProfessionalsEmail
				{
					FormElements = new Dictionary<string, string>{
										{"Contact Info", model.ContactInfo!=null?string.Join(", ", model.ContactInfo) :string.Empty},
										{"Name", model.Name},
										{"Title", model.Title},
										{"Company", model.Company},
										{"Phone Number", model.Phone},
										{"Email Address", model.Email},
										{"Street Address", model.StreetAddress},
										{"City", model.City},
										{"State", model.State},
										{"ZIP Code", model.Zip}
								},
					To = ConfigurationManager.AppSettings.Get("Email.YoungProfessionals.To")
				};
                email.Send();
				return PartialView("_YoungProfessionalsThankYou");
			}
            return View(model);
		}
	}
}
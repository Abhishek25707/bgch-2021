﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BGCH.Controllers
{
    public class EventsController : Controller
    {
        //Subpages for "Events" header
        public ActionResult Santa_Project() { return View(); }
        public ActionResult Great_Futures_Dinner() { return View(); }
        public ActionResult Sporting_Clays_Tournament() { return View(); }
        public ActionResult Rise_And_Shine_Breakfast() { return View(); }
        public ActionResult Calendar() { return View(); }
        public ActionResult Other_Events() { return View(); }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BGCH.Controllers
{
    public class NewsAndMediaController : Controller
    {
		//Subpages for "News & Media" header
        public ActionResult Newsletter() { return View(); }
        public ActionResult In_The_News() { return View(); }
    }
}
﻿using System.Web.Mvc;

namespace BGCH.Controllers
{
    public class AboutUsController : Controller
    {
        //Subpages for "About Us" header
        public ActionResult About_Us() { return View(); }
        public ActionResult Mission_And_History() { return View(); }
        public ActionResult Our_Team() { return View(); }
        public ActionResult Club_Locations() { return View(); }
        public ActionResult Careers() { return View(); }
        public ActionResult Executive_Committee_And_Board() { return View(); }

    }
}
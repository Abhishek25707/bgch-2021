﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using BGCH.Models;

namespace BGCH.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index() { return View(); }
        public ActionResult Leclair_Barratt() { return View(); }        
        public ActionResult News_And_Events() { return View(); }
        public ActionResult Our_Clubs() { return View(); }
        public ActionResult Summer_Learning_Loss() { return View(); }
        public ActionResult Donor_Policy() { return View(); }        
        public ActionResult Donate_Gfd() { return View(); }
        public ActionResult Best_Buy_Teen_Tech_Center() { return View(); }
        public ActionResult Sitemap() { return View(); }
        
    }
}
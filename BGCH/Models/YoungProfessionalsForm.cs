﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BGCH.Models
{
    public class YoungProfessionalsForm
    {
        public List<string> ContactInfo { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [StringLength(100)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [StringLength(100)]
        [Display(Name = "Company")]
        public string Company { get; set; }

        [Required]
        [StringLength(20)]
        [Phone]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        [StringLength(50)]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(100)]
        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(2)]
        public string State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }
    }
}
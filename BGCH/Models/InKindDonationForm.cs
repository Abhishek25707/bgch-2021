﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BGCH.Models
{
	public class InKindDonationForm
	{
		public List<string> DonatedItems { get; set; }

		[StringLength(50)]
		[Display(Name = "Estiamted Fair Market Value")]
		public string EstimatedFairMarketValue { get; set; }

		[StringLength(500)]
		[DataType(DataType.MultilineText)]
		[Display(Name = "Method Used to Estimate")]
		public string MethodUsedToEstimate { get; set; }

		[Required]
		[DataType(DataType.Date)]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
		[Display(Name = "Date of Donation")]
		public DateTime? DateOfDonation { get; set; }

		[Required]
		[StringLength(100)]
		[Display(Name = "Donor Name")]
		public string DonorName { get; set; }

		[StringLength(100)]
		[Display(Name = "Contact and Title")]
		public string ContactAndTitle { get; set; }

		[Required]
		[StringLength(20)]
		[Phone]
		[DataType(DataType.PhoneNumber)]
		public string Phone { get; set; }

		[Required]
		[StringLength(50)]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[StringLength(500)]
		[DataType(DataType.MultilineText)]
		[Display(Name = "Special Instructions")]
		public string SpecialInstructions { get; set; }

		[StringLength(100)]
		[Display(Name = "Street Address")]
		public string StreetAddress { get; set; }

		[StringLength(50)]
		public string City { get; set; }

		[StringLength(2)]
		public string State { get; set; }

		[StringLength(10)]
		public string Zip { get; set; }

		[StringLength(500)]
		[DataType(DataType.MultilineText)]
		[Display(Name = "Other Items to Donate")]
		public string OtherItemsToDonate { get; set; }
	}
}
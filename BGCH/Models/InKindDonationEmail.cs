﻿using System.Collections.Generic;
using Postal;


namespace BGCH.Models
{
	public class InKindDonationEmail : Email
	{
		public string To { get; set; }
		public string Subject { get; set; }
		public Dictionary<string, string> FormElements { get; set; }
	}
}
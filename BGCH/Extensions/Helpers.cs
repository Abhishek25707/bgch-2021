﻿using System;
public static class Helpers
{
	public static DateTime Now(string TimeZoneID)
	{
		DateTime utcTime = DateTime.UtcNow;
		TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneID);
		return TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);
	}

	public static DateTime Now()
	{
		return Now("Eastern Standard Time");
	}
}
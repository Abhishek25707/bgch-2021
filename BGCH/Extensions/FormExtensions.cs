﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

public static class FormExtensions
{

	public static Dictionary<string, string> stateList
	{
		get
		{
			return new Dictionary<string, string>()
						{
								{String.Empty, String.Empty},
								{"AL","Alabama"},
								{"AK","Alaska"},
								{"AZ","Arizona"},
								{"AR","Arkansas"},
								{"CA","California"},
								{"CO","Colorado"},
								{"CT","Connecticut"},
								{"DE","Delaware"},
								{"FL","Florida"},
								{"GA","Georgia"},
								{"HI","Hawaii"},
								{"ID","Idaho"},
								{"IL","Illinois"},
								{"IN","Indiana"},
								{"IA","Iowa"},
								{"KS","Kansas"},
								{"KY","Kentucky"},
								{"LA","Louisiana"},
								{"ME","Maine"},
								{"MD","Maryland"},
								{"MA","Massachusetts"},
								{"MI","Michigan"},
								{"MN","Minnesota"},
								{"MS","Mississippi"},
								{"MO","Missouri"},
								{"MT","Montana"},
								{"NE","Nebraska"},
								{"NV","Nevada"},
								{"NH","New Hampshire"},
								{"NJ","New Jersey"},
								{"NM","New Mexico"},
								{"NY","New York"},
								{"NC","North Carolina"},
								{"ND","North Dakota"},
								{"OH","Ohio"},
								{"OK","Oklahoma"},
								{"OR","Oregon"},
								{"PA","Pennsylvania"},
								{"RI","Rhode Island"},
								{"SC","South Carolina"},
								{"SD","South Dakota"},
								{"TN","Tennessee"},
								{"TX","Texas"},
								{"UT","Utah"},
								{"VT","Vermont"},
								{"VA","Virginia"},
								{"WA","Washington"},
								{"WV","West Virginia"},
								{"WI","Wisconsin"},
								{"WY","Wyoming"},
								{"AS","American Samoa"},
								{"CN","Canada"},
								{"DC","District of Columbia"},
								{"FM","Federated States of Micronesia"},
								{"MH","Marshall Islands"},
								{"MP","Northern Mariana Islands"},
								{"PW","Palau"},
								{"PR","Puerto Rico"},
								{"VI","Virgin Islands"},
								{"GU","Guam"}
						};
		}
	}

	public static MvcHtmlString StateDropDownList(this HtmlHelper html, string name)
	{
		return html.DropDownList(name, new SelectList(stateList, "key", "value"));
	}

	public static MvcHtmlString StateDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string value = null, bool useAbbreviation = false, string optionLabel = null)
	{
		if (useAbbreviation)
		{
			//return html.DropDownListFor(expression, new SelectList(stateList, "key", "key"), optionLabel);
			return html.DropDownListFor(expression, new SelectList(stateList, "key", "key", value == null ? null : value), optionLabel);
		}
		return html.DropDownListFor(expression, new SelectList(stateList, "key", "value"), optionLabel);
	}


	public static MvcHtmlString EditorWithPlaceholderFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string placeholder = null)
	{
		return placeholder == null ? html.EditorFor(expression) : html.EditorFor(expression, new { htmlAttributes = new { placeholder = placeholder } });
	}
}